const mongoose = require("mongoose");
const Course = require ("../models/course.js");

// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})

// 	return newCourse.save().then((newCourse, error) => {
// 		if (error) {
// 			return error;
// 		}
// 		else{
// 			return newCourse;
// 		}
// 	})
// }


module.exports.addCourse = (reqBody, newData) => {
	if(newData.isAdmin == true){
		let newCourse = new Course({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})
		return newCourse.save().then((newCourse, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
	

}



// Function for adding a course
// 2. Update the "addCourse" controller method to implement admin authentication for creating a course.


// GET all course
module.exports.getAllCourse = () => {
	return Course.find({}).then(result =>{
		return result;
	})
}

// GET active course
module.exports.getActiveCourse = () => {
	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}

module.exports.getCourse = (courseId) => {
	return Course.find({courseId}).then(result =>{
		return result;
	})
}

// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				// newData.course.name
				// newData.request.body.name
				name: newData.course.name, 
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
	

}


// s-40 activity

module.exports.courseToArchive = (courseId, archiveCourse) => {
	if(archiveCourse.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: archiveCourse.course.isActive
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
	

}

