const express = require("express");
const router = express.Router();
const courseControllers =  require("../controllers/courseControllers.js");
const auth = require("../auth.js");

// Creating a course
// Activity
// 1. Update the "course" route to implement "user authentication for the admin" when creating a course.
// router.post("/create", (request, response) => {
// 	courseControllers.addCourse(request.body).then(resultFromController => response.send(resultFromController))
// });


router.post("/create", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	courseControllers.addCourse(request.body, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


// Get all courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourse().then(resultFromController => response.send(resultFromController))
});

// [Additional Example]
// [Arrow function to regular function]
// Get ALL course in Regular function
router.get("/getAllCoursesv2", (request, response) => {
	courseControllers.getAllCourseInRegularFunction()
	.then(
		function getResultFromController(resultFromController) { 
			response.send(resultFromController)
		}
	);
});


// Get all ACTIVE courses
router.get("/active", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromController => response.send(resultFromController))
});


// Get SPECIFIC course
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController));
})

// [Additional Example]
// [Arrow function to regular function]
// Get SPECIFIC course
router.get("/:courseId/getSpecificV2", (request, response) => {
	courseControllers.GetSpecificCourseInRegularFunction(request.params.courseId)
	.then(
		function getResultFromController(resultFromController){
		 	response.send(resultFromController)
		}
	);
})

router.patch("/:courseId/update", auth.verify, (request,response) => 
{
	const newData = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})


// s-40 activity
router.patch("/:courseId/archive", auth.verify, (request,response) => 
{
	const archiveCourse = {
		course: request.body, 	//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin 
	}

	courseControllers.courseToArchive(request.params.courseId, archiveCourse).then(resultFromController => {
		response.send(resultFromController)
	})
})







module.exports = router;
