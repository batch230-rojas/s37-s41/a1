const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController.js");

router.post("/register", (request, response) =>{
	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController));
})

router.post("/checkEmail", (request, response) => {
	userController.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})


// s38 Activity solution
router.post("/details", (request, response) => {
	userController.getProfile(request.body).then(resultFromController => response.send(resultFromController))
})




module.exports = router; 

